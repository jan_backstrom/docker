#!/usr/bin/env bash
docker run --rm -d --name sonar-7.9.1 -p 9000:9000 --mount source=postgres-sonar-data,target=/var/lib/postgresql/data docker.quickspin.io/sonar:7.9.1
