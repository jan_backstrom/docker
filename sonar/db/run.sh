#!/usr/bin/env bash
docker run --rm -d --name postgres-sonar-9.6.1 -p 5430:5432 --mount source=postgres-sonar-data,target=/var/lib/postgresql/data docker.quickspin.io/postgres-sonar:9.6.1
