#!/bin/sh

set -e
>&2 echo "Postgres is starting"

# until PGPASSWORD=sonar psql -h localhost -U sonar -d sonarqube -c '\q'; do
#  >&2 echo "Postgres is unavailable - sleeping"
#  sleep 1
# done
sleep 10

>&2 echo "Postgres is up"

